package me.l3m4rk.tanukiapp

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.widget.Toast
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import me.l3m4rk.tanukiapp.data.TanukiApi
import me.l3m4rk.tanukiapp.model.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var v: String

    @Inject
    lateinit var api: TanukiApi

    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sendOrderButton.setOnClickListener { handleSendOrderClick() }
    }

    private fun handleSendOrderClick() {
        progress.visibility = View.VISIBLE
        disposables.clear()
        disposables += bindSendOrder()
    }

    private fun bindSendOrder(): Disposable {
        return api.postOrder(
            OrderRequest(
                data = OrderRequest.Data(
                    comments = "Комментарий",
                    orderItems = arrayListOf(
                        OrderItem(
                            amount = 4
                        )
                    ),
                    persons = 2,
                    sender = Sender(
                        name = "Михаил",
                        phone = "+7 (903) 539-32-11"
                    ),
                    paymentMethod = PaymentMethod.CASH.method,
                    deliveryAddress = DeliveryAddress(street = "Буракова", house = "6", apartment = "52")
                )
            )
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { progress.visibility = GONE }
            .doOnError { progress.visibility = GONE }
            .subscribe({
                handleSuccess(it.Response.Result.message)
            }, { handleError(it) })
    }

    private fun handleSuccess(message: String) {
        Toast.makeText(this, "Your order has been placed! \n$message", Toast.LENGTH_LONG).show()
    }

    private fun handleError(e: Throwable) {
        Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }
}
