package me.l3m4rk.tanukiapp.di

import android.content.Context
import dagger.Binds
import dagger.Module
import me.l3m4rk.tanukiapp.TanukiApp

@Module
abstract class AppModule {

    @Binds
    abstract fun context(app: TanukiApp): Context

}