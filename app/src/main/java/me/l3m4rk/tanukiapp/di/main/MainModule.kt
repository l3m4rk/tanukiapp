package me.l3m4rk.tanukiapp.di.main

import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    fun testProvide(): String = "42"

}