package me.l3m4rk.tanukiapp.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import me.l3m4rk.tanukiapp.MainActivity
import me.l3m4rk.tanukiapp.di.main.MainModule

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun bindMainActivity(): MainActivity

}