package me.l3m4rk.tanukiapp.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import me.l3m4rk.tanukiapp.TanukiApp
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ActivitiesModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        NetworkModule::class
    ]
)
interface AppComponent : AndroidInjector<TanukiApp> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: TanukiApp): Builder

        fun build(): AppComponent
    }
}