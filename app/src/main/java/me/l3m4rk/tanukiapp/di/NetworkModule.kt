package me.l3m4rk.tanukiapp.di

import dagger.Module
import dagger.Provides
import me.l3m4rk.tanukiapp.data.TanukiApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

private const val BASE_URL = "http://api.dev2.tanuki.ru"

@Module
class NetworkModule {


    @Provides
    fun provideService(): TanukiApi {

        val client = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .build()

        return Retrofit.Builder()
            .apply {
                addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                addConverterFactory(MoshiConverterFactory.create())
                baseUrl(BASE_URL)
                client(client)
            }
            .build()
            .create(TanukiApi::class.java)
    }
}