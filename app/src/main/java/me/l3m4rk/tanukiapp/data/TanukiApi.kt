package me.l3m4rk.tanukiapp.data

import io.reactivex.Single
import me.l3m4rk.tanukiapp.model.OrderRequest
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface TanukiApi {

    @POST("/")
    @Headers("User-Agent: Tanuki/Test_app")
    @FormUrlEncoded
    fun postOrder(@Field("jsonData") request: OrderRequest): Single<OrderResponse>


}

data class OrderResponse(
    val Response: ResponseItem,
    val ResponseBody: ResponseBodyItem
) {
    data class ResponseItem(
        val method: String,
        val Result: CallResult
    ) {
        data class CallResult(
            val code: String,
            val message: String
        )
    }

    data class ResponseBodyItem(
        val ValidationResults: Results,
        val OrderInfo: OrderInfoDTO?
    ) {
        data class Results(val Result: Int, val Errors: List<ApiError>?)

        data class ApiError(val Code: Int, val ErrorMessage: String)

        data class OrderInfoDTO(
            val orderNumber: String,
            val messageTitle: String,
            val message: String,
            val creationDate: String
        )
    }
}
