package me.l3m4rk.tanukiapp

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import me.l3m4rk.tanukiapp.di.DaggerAppComponent

class TanukiApp : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<TanukiApp> {
        return DaggerAppComponent.builder()
            .application(this)
            .build()
    }

}