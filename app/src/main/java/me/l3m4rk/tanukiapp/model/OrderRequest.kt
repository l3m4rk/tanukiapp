package me.l3m4rk.tanukiapp.model


data class OrderRequest(
    val data: Data
) {
    data class Data(
        val deliveryType: String = "deliveryTypeRegular",
        val comments: String,
        val orderItems: List<OrderItem>,
        val persons: Int,
        val paymentMethod: String,
        val notificationType: String = "СМС оповещение",
        val deliveryAddress: DeliveryAddress,
        val sender: Sender,
        val method: Method = Method(),
        val header: Header = Header()
    )
}

data class OrderItem(
    val itemId: String = "9",
    val amount: Int,
    val price: Int = 110
)

data class DeliveryAddress(
    val cityId: Int = 1,
    val street: String,
    val house: String,
    val apartment: String
)

data class Sender(
    val name: String,
    val phone: String
)

data class Method(
    val name: String = "makeOrder",
    val mode: String = "getData",
    val mtime: Int = 0
)

data class Header(
    val version: String = "2.0",
    val userId: String = "b1fd5271d981d1b7",
    val debugMode: Boolean = true,
    val agent: Agent = Agent()
) {
    data class Agent(
        val device: String = "desktop",
        val version: String = "Chromium 68.0.3440.75"
    )
}

enum class PaymentMethod(val method: String) {

    CASH("payment_encash"),
    CARD("payment_card_restaurant")

}